// owl carousel 

$(document).ready(function(){
  $(function() {
      // Owl Carousel
      var owl = $(".owl-carousel");
      owl.owlCarousel({
          items: 3,
          margin: 20,
          autoplay:true,
          autoplayTimeOUt:2000,
          autoplayHOverPause:true,
          loop: true,
          nav: true,
          responsive: {
              0: {
                  items: 1,
                  nav: false
              },
              600: {
                  items: 2,
                  nav: false
              },
              1000: {
                  items: 3,
                  nav: false
              },
  
          }
      });
  });
});

// owl carousel end  

// nav js start 

// define all UI variable
const navToggler = document.querySelector('.nav-toggler');
const navMenu = document.querySelector('.site-navbar ul');
const navLinks = document.querySelectorAll('.site-navbar a');

// load all event listners
allEventListners();

// functions of all event listners
function allEventListners() {
  // toggler icon click event
  navToggler.addEventListener('click', togglerClick);
  // nav links click event
  navLinks.forEach( elem => elem.addEventListener('click', navLinkClick));
}

// togglerClick function
function togglerClick() {
  navToggler.classList.toggle('toggler-open');
  navMenu.classList.toggle('open');
}

// navLinkClick function
function navLinkClick() {
  if(navMenu.classList.contains('open')) {
    navToggler.click();
  }
}
// nav js end 


// Scroll to active menu nav js 

console.log("reading menu active script");
const sections = document.querySelectorAll("section.anchor");
const navLi = document.querySelectorAll(
"#navbarCollapse .navbar-nav ul li"
);
window.onscroll = () => {
debonuceActivateMenu()
};

function debounce(func, timeout = 50) {
// console.log('debounce inside')
let timer;
return () => {
  // console.log(timer)
  clearTimeout(timer);
  timer = setTimeout(() => {
    func.apply(this);
  }, timeout);
};
}

function activateMenu () {     
console.log('activate Menu')   
var current = "";

sections.forEach((section) => {
  const sectionTop = section.offsetTop;
  if (scrollY >= sectionTop - 60) {
    current = section.getAttribute("id");
  }
});
console.log("checking id", current);

navLi.forEach((li) => {
  li.classList.remove("active");
  if (li.classList.contains(current)) {
    li.classList.add("active");
  }
});
}
const debonuceActivateMenu = debounce(() => activateMenu());
// Scroll to active menu nav js 

// projects/portfolio section tabs js start

// this js file is commented , if you needed you can use it by uncomment it. 

// function Tabs() {
//   var bindAll = function () {
//     var menuElements = document.querySelectorAll("[data-tab]");
//     for (var i = 0; i < menuElements.length; i++) {
//       menuElements[i].addEventListener("click", change, false);
//     }
//   };
//   var clear = function () {
//     var menuElements = document.querySelectorAll("[data-tab]");
//     for (var i = 0; i < menuElements.length; i++) {
//       menuElements[i].classList.remove("active");
//       var id = menuElements[i].getAttribute("data-tab");
//       document.getElementById(id).classList.remove("active");
//     }
//   };
//   var change = function (e) {
//     clear();
//     e.target.classList.add("active");
//     var id = e.currentTarget.getAttribute("data-tab");
//     document.getElementById(id).classList.add("active");
//   };
//   bindAll();
// }
// var connectTabs = new Tabs();

// projects/portfolio section tabs js end